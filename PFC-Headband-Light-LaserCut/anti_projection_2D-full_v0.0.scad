/*
//* FILE   : anti_projection_2D-full_v0.scad
//* AUTHOR : Denis Terwagne
//* DATE   : 2020-03-20
//* Version: 0.0 (first draft)
//* Notes  : inspired from the 3D model of Nicolas De Coster
//* License : M.I.T. (https://opensource.org/licenses/MIT)
//*
//*/

//tolerance
t=0;

out_d   = 30; //120;  //inside diameter of outer ring
in_d    = 100; //100;  //inside diameter of inner ring
length_ir = 220; //length inner ring
length_or_slots = 280; //length outer ring to slots
length_or = 300; //length outer ring
height  = 10;   //height of the module
//sec     = 5;    //setback from joining branches
th      = 1;    //thickness of module
n_holes = 10;   //number of holes in inner ring
//hook_d  = 10;   //hooks diameter
height_slot = height/2;
width_slot = th*2;
length_tab = 2; //length of the tab

holes_d = height/2;

// Inner ring

translate([0,20,0]){

difference(){
    union(){
cube([length_ir/2,height,th]);
translate([0,height/4,0])cube([length_ir/2+length_tab,height_slot-t,th]);
    
translate([length_ir/2+length_tab,0,0])
difference(){
translate([0,height/2,0])cylinder(r=height/2,$fn=100);
translate([-height/2,0,0])cube([height/2,height,th]);
}
}

for(i=[0:n_holes]){
      translate([i*length_ir/(n_holes+1)/2,height/2,0])
      cylinder(d=holes_d, $fn=6);
      }
}

mirror([1,0,0]){

difference(){
    union(){
cube([length_ir/2,height,th]);
translate([0,height/4,0])cube([length_ir/2+length_tab,height_slot-t,th]);
    
translate([length_ir/2+length_tab,0,0])
difference(){
translate([0,height/2,0])cylinder(r=height/2,$fn=100);
translate([-height/2,0,0])cube([height/2,height,th]);
}
}

for(i=[0:n_holes]){
      translate([i*length_ir/(n_holes+1)/2,height/2,0])
      cylinder(d=holes_d, $fn=6);
      }
}
}
}

//Outer ring

difference(){
union(){
cube([length_or/2,height,th]);
translate([0,height/4,0])cube([length_or/2+length_tab,height_slot-t,th]);
translate([length_or/2+length_tab,0,0]) difference(){
translate([0,height/2,0])cylinder(r=height/2,$fn=100);
translate([-height/2,0,0])cube([height/2,height,th]);
}
}
translate([length_or_slots/2-height+width_slot-t,height/2-width_slot/2,0])
    cube([height+t,width_slot,th]);     
translate([length_or_slots/2,height/4,0]) cube([length_tab,height_slot+t,th]);    
}

mirror([1,0,0]){
    difference(){
union(){
cube([length_or/2,height,th]);
translate([0,height/4,0])cube([length_or/2+length_tab,height_slot-t,th]);
translate([length_or/2+length_tab,0,0]) difference(){
translate([0,height/2,0])cylinder(r=height/2,$fn=100);
translate([-height/2,0,0])cube([height/2,height,th]);
}
}
translate([length_or_slots/2-height+width_slot-t,height/2-width_slot/2,0])
    cube([height+t,width_slot,th]);     
translate([length_or_slots/2,height/4,0]) cube([length_tab,height_slot+t,th]);    
}
}